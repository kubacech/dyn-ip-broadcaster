# dyn-ip-broadcaster  
Simple broadcasting IP address with HTTP POST or ssh to file. Can be useful for keeping track of dynamic IP.

Two types of usage are now supported:

```bash
# HTTP POST
broadcast-ip --curl http://yourweb.site/some/path

# SSH
boradcast-ip --ssh user@machine:/your/file
```
