#/bin/bash

function curl_call {
	curl --silent --fail --request POST "${PATH}" --data "${IP}"
}

function ssh_call {
	# split ssh path by : for use in following command
	IFS=':' read -ra SPLIT_PATH <<< "${PATH}"
	if [ "${#SPLIT_PATH[@]}" -ne 2 ]; then
    	echo "Wrong input - expected ssh path as login@ssh_server:/path/to/file"
    	exit 1
	fi
	echo ${IP} | ssh ${ARRAY[0]} "cat > ${ARRAY[1]}"
}

case $1 in
	-C|--curl)
	METHOD='curl_call'
	;;
	-S|--ssh)
	METHOD='ssh_call' 
	;;
esac

PATH=$2
IP=$(curl -s ifconfig.me)

if ! [[ $IP =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
	echo "wrong IP format obtained: ${IP}"
	exit 1
fi

echo "IP=${IP}"
eval ${METHOD}
RES=$?

if [[ ${RES} -ne 0 ]]; then
	echo "FAIL, command exited with result code ${RES}"
else
	echo "SUCCESS"
fi
exit ${RES}

